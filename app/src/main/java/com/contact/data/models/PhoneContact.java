package com.contact.data.models;

import android.os.Parcel;
import android.os.Parcelable;

public class PhoneContact implements Parcelable {

    private String name;
    private String phoneNumber;

    public PhoneContact(String name, String number) {
        this.name = name;
        this.phoneNumber = number;
    }

    public PhoneContact(Parcel in) {
        name = in.readString();
        phoneNumber = in.readString();
    }

    public static final Creator<PhoneContact> CREATOR = new Creator<PhoneContact>() {
        @Override
        public PhoneContact createFromParcel(Parcel in) {
            return new PhoneContact(in);
        }

        @Override
        public PhoneContact[] newArray(int size) {
            return new PhoneContact[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(phoneNumber);
    }
}
