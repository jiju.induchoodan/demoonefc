package com.contact.data;

import com.contact.data.models.PhoneContact;
import com.contact.util.threading.MainUiThread;
import com.contact.util.threading.ThreadExecutor;

import java.util.List;

public abstract class DataSource {

    protected MainUiThread mainUiThread;
    protected ThreadExecutor threadExecutor;

    public DataSource(MainUiThread mainUiThread, ThreadExecutor threadExecutor) {
        this.mainUiThread = mainUiThread;
        this.threadExecutor = threadExecutor;
    }

    public interface GetContactsCallback {
        void onSuccess(List<PhoneContact> contacts);
        void onFailure(Throwable throwable);
    }

    public abstract void getContacts(GetContactsCallback callback);

}
