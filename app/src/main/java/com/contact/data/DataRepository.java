package com.contact.data;

import com.contact.data.models.PhoneContact;

import java.util.List;

public class DataRepository {

    private DataSource localDataSource;

    private static DataRepository dataRepository;

    public static synchronized DataRepository getInstance(DataSource localDataSource) {
        if (dataRepository == null) {
            dataRepository = new DataRepository(localDataSource);
        }
        return dataRepository;
    }

    public DataRepository(DataSource localDataSource) {
        this.localDataSource = localDataSource;
    }

    public void fetchContacts(final LocalDataSource.GetContactsCallback callback) {
        localDataSource.getContacts(new DataSource.GetContactsCallback() {
            @Override
            public void onSuccess(List<PhoneContact> contacts) {
                callback.onSuccess(contacts);
            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });
    }
}
