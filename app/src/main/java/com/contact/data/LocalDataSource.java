package com.contact.data;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import com.contact.data.models.PhoneContact;
import com.contact.util.threading.MainUiThread;
import com.contact.util.threading.ThreadExecutor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class LocalDataSource extends DataSource {

    private static LocalDataSource localDataSource;
    private static Context context;
    private static ThreadExecutor threadExecutor;
    private static MainUiThread mainUiThread;

    private LocalDataSource(MainUiThread mainUiThread, ThreadExecutor threadExecutor, Context context) {
        super(mainUiThread, threadExecutor);
        this.mainUiThread = mainUiThread;
        this.threadExecutor = threadExecutor;
        this.context = context;
    }

    public static synchronized LocalDataSource getInstance(MainUiThread mainUiThread,
                                                           ThreadExecutor threadExecutor, Context context) {
        if (localDataSource == null) {
            localDataSource = new LocalDataSource(mainUiThread, threadExecutor, context);
        }
        return localDataSource;
    }

    @Override
    public void getContacts(final GetContactsCallback callback) {
        final List<PhoneContact> contactsList = new ArrayList<PhoneContact>();

        threadExecutor.execute(new Runnable() {
            @Override
            public void run() {
                ContentResolver contentResolver = context.getContentResolver();
                String[] projection = new String[]{
                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                        ContactsContract.CommonDataKinds.Phone.NUMBER,
                        ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER,
                };

                Cursor cursor = null;
                try {
                    cursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
                } catch (SecurityException e) {
                    //SecurityException can be thrown if we don't have the right permissions
                }

                if (cursor != null) {
                    try {
                        HashSet<String> normalizedNumbersAlreadyFound = new HashSet<>();
                        int indexOfNormalizedNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER);
                        int indexOfDisplayName = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                        int indexOfDisplayNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

                        while (cursor.moveToNext()) {
                            String normalizedNumber = cursor.getString(indexOfNormalizedNumber);
                            if (normalizedNumbersAlreadyFound.add(normalizedNumber)) {
                                String displayName = cursor.getString(indexOfDisplayName);
                                String displayNumber = cursor.getString(indexOfDisplayNumber);
                                PhoneContact contact = new PhoneContact(displayName,displayNumber);
                                contactsList.add(contact);
                            }
                        }
                    } finally {
                        cursor.close();
                    }
                }
                mainUiThread.post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onSuccess(contactsList);
                    }
                });
            }
        });
    }

}
