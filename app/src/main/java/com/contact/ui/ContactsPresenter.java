package com.contact.ui;

import android.content.Context;
import com.contact.data.DataRepository;
import com.contact.data.DataSource;
import com.contact.data.models.PhoneContact;
import com.contact.util.mvp.BasePresenter;

import java.util.List;

public class ContactsPresenter extends BasePresenter<ContactsContractor.View> implements ContactsContractor.Presenter {

    private DataRepository dataRepository;
    private Context context;

    public ContactsPresenter(ContactsContractor.View view, DataRepository dataRepository, Context context) {
        this.dataRepository = dataRepository;
        this.view = view;
        this.context = context;

    }

    @Override
    public void fetchContacts() {
        view.setProgressBar(true);
        dataRepository.fetchContacts(new DataSource.GetContactsCallback() {
            @Override
            public void onSuccess(List<PhoneContact> contacts) {
                if (contacts.size() > 0) {
                    view.populateList(contacts);
                    view.setProgressBar(false);
                } else
                    view.setProgressBar(true);
            }

            @Override
            public void onFailure(Throwable throwable) {
                //TODO:  Failure condition to handle
            }
        });
    }
}
