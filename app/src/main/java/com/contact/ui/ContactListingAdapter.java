package com.contact.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.contact.data.models.PhoneContact;
import contact.demo.com.contact.R;

import java.util.List;

public class ContactListingAdapter extends RecyclerView.Adapter<ContactListingAdapter.ViewHolder> {

    private List<PhoneContact> contacts;

    public ContactListingAdapter(List<PhoneContact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.contact_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PhoneContact contact = contacts.get(position);
        holder.textViewName.setText(contact.getName());
        holder.textViewNumber.setText(contact.getPhoneNumber());
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewNumber;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewNumber = itemView.findViewById(R.id.textViewNumber);
        }
    }

    public void clear() {
        contacts.clear();
    }

    public void addAll(List<PhoneContact> contacts) {
        this.contacts.addAll(contacts);
        notifyDataSetChanged();
    }
}
