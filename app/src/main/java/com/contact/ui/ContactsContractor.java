package com.contact.ui;

import com.contact.data.models.PhoneContact;
import com.contact.util.mvp.IBasePresenter;
import com.contact.util.mvp.IBaseView;

import java.util.List;

public interface ContactsContractor {

    interface View extends IBaseView {

        void populateList(List<PhoneContact> contacts);
    }

    interface Presenter extends IBasePresenter<View> {

        void fetchContacts();
    }
}
