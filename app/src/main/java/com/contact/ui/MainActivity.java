package com.contact.ui;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.contact.data.DataRepository;
import com.contact.data.LocalDataSource;
import com.contact.data.models.PhoneContact;
import com.contact.util.MCompatibilityUtil;
import com.contact.util.threading.MainUiThread;
import com.contact.util.threading.ThreadExecutor;
import contact.demo.com.contact.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ContactsContractor.View {
    private ContactsContractor.Presenter presenter;
    DataRepository dataRepository;
    MainUiThread mainUiThread;
    ThreadExecutor executor;
    ContactListingAdapter adapter;
    RecyclerView recyclerView;
    private List<PhoneContact> phoneContacts;
    ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        phoneContacts = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerViewContact);
        progressBar = findViewById(R.id.progressBar);

        adapter = new ContactListingAdapter(phoneContacts);
        recyclerView.setAdapter(adapter);

        executor = ThreadExecutor.getInstance();
        mainUiThread = MainUiThread.getInstance();

        dataRepository = DataRepository.getInstance(LocalDataSource.getInstance(mainUiThread, executor, this));

        presenter = new ContactsPresenter(this, dataRepository, this);
        if (!MCompatibilityUtil.isPermissionEnabled(this, Manifest.permission.READ_CONTACTS))
            MCompatibilityUtil.requestAppPermission(this, new String[]{Manifest.permission.READ_CONTACTS}, MCompatibilityUtil.REQUEST_CODE_CONTACTS);
        else
            presenter.fetchContacts();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MCompatibilityUtil.REQUEST_CODE_CONTACTS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (presenter != null)
                        presenter.fetchContacts();
                    else {
                        presenter = new ContactsPresenter(this, dataRepository, this);
                        presenter.fetchContacts();
                    }
                } else {
                    showToastMessage("App will display contacts only after allowing this permission");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    }, 2500);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void populateList(List<PhoneContact> contacts) {
        recyclerView.setVisibility(View.VISIBLE);
        adapter.clear();
        adapter.addAll(contacts);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setProgressBar(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public Context getContext() {
        return this;
    }
}