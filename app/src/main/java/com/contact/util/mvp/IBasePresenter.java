package com.contact.util.mvp;

public interface IBasePresenter<View> {

    void onViewActive(View view);

    void onViewInactive();
}
