package com.contact.util.mvp;


/**
 * Abstract base class to be extended by any MVP Presenter. It contains common attributes and
 * methods to be shared across Presenters.
 *
 * @param <View> a generic type to indicate a type of MVP View
 */
public class BasePresenter<View> implements IBasePresenter<View> {

    protected View view;

    @Override
    public void onViewActive(View view) {
        this.view = view;
    }

    @Override
    public void onViewInactive() {
        view = null;
    }
}
