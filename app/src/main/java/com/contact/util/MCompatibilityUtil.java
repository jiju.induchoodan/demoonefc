package com.contact.util;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

public class MCompatibilityUtil {

    public static final int REQUEST_CODE_CONTACTS = 101;

    public static boolean isPermissionEnabled(Activity activity, String permission) {
        if (ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED)
            return true;
        else
            return false;
    }

    public static void requestAppPermission(final Activity activity, final String[] permission, final int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission[0])) {
            Toast.makeText(activity, "App will display contacts only after allowing this permission", Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(activity, permission, requestCode);
            return;
        }
        ActivityCompat.requestPermissions(activity, permission, requestCode);
        return;
    }
}
